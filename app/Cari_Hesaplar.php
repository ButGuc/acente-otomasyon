<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cari_Hesaplar extends Model
{
    protected $primaryKey = 'ch_id';
    protected $table = 'cari_hesaplar';

    public $fillable = [
        'ch_adi','ch_doviz'
    ];

    public function Odemeler() {
        return $this->hasMany('App\Odemeler','ch_id');
    }
}
