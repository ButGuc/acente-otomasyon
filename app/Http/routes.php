<?php

Route::pattern('id', '[0-9]+');

Route::auth();

if (Auth::guest()) {
    Route::get('/', function () {
        return redirect('/login');
    });
    Route::get('/register', function () {
        return redirect('/login');
    });
    Route::post('/register', function () {
        return redirect('/login');
    });
}
else {
    Route::get('/', function () {
        return redirect('/panel');
    });
}

Route::get('/panel', 'panelController@index');
Route::get('/panel/sistemayarlari', 'panelController@sistemayarlari');
Route::get('/panel/dovizDetay', 'panelController@dovizDetay');
Route::get('/panel/cariler', 'panelController@cariler');
Route::get('/panel/istatistikler', 'panelController@istatistikler');
Route::get('/panel/istatistikler/filitreli', 'panelController@istatistiklerFilitreli');

Route::get('/panel/oteller', 'otelController@index');
Route::get('/panel/otel/ekle', 'otelController@ekleForm');
Route::post('/panel/otel/ekle', 'otelController@eklePost');
Route::get('/panel/otel/duzenle/{id}', 'otelController@duzenleForm');
Route::post('/panel/otel/duzenle/{id}', 'otelController@duzenlePost');
Route::get('/panel/otel/sil/{id}', 'otelController@sil');

Route::get('/panel/turlar', 'turController@index');
Route::get('/panel/tur/ekle', 'turController@ekleForm');
Route::post('/panel/tur/ekle', 'turController@eklePost');
Route::get('/panel/tur/duzenle/{id}', 'turController@duzenleForm');
Route::post('/panel/tur/duzenle/{id}', 'turController@duzenlePost');
Route::get('/panel/tur/sil/{id}', 'turController@sil');

Route::get('/panel/rehberler', 'rehberController@index');
Route::get('/panel/rehber/ekle', 'rehberController@ekleForm');
Route::post('/panel/rehber/ekle', 'rehberController@eklePost');
Route::get('/panel/rehber/duzenle/{id}', 'rehberController@duzenleForm');
Route::post('/panel/rehber/duzenle/{id}', 'rehberController@duzenlePost');
Route::get('/panel/rehber/sil/{id}', 'rehberController@sil');

Route::get('/panel/asb', 'asbController@index');
Route::get('/panel/asb/ekle', 'asbController@ekleForm');
Route::post('/panel/asb/ekle', 'asbController@eklePost');
Route::get('/panel/asb/duzenle/{id}', 'asbController@duzenleForm');
Route::post('/panel/asb/duzenle/{id}', 'asbController@duzenlePost');
Route::get('/panel/asb/sil/{id}', 'asbController@sil');

Route::get('/panel/cari/hesaplar', 'cariHesapController@index');
Route::get('/panel/cari/hesap/ekle', 'cariHesapController@ekleForm');
Route::post('/panel/cari/hesap/ekle', 'cariHesapController@eklePost');
Route::get('/panel/cari/hesap/duzenle/{id}', 'cariHesapController@duzenleForm');
Route::post('/panel/cari/hesap/duzenle/{id}', 'cariHesapController@duzenlePost');
Route::get('/panel/cari/hesap/sil/{id}', 'cariHesapController@sil');

Route::get('/panel/odemeler', 'odemeController@index');
Route::get('/panel/odemeler/filitreli', 'odemeController@indexFilitreli');
Route::get('/panel/odeme/ekle', 'odemeController@ekleForm');
Route::post('/panel/odeme/ekle', 'odemeController@eklePost');
Route::get('/panel/odeme/duzenle/{id}', 'odemeController@duzenleForm');
Route::post('/panel/odeme/duzenle/{id}', 'odemeController@duzenlePost');
Route::get('/panel/odeme/sil/{id}', 'odemeController@sil');
Route::get('/panel/odeme/excel/', 'odemeController@excelOut');

Route::get('/panel/rezervasyonlar', 'rezervasyonController@index');
Route::get('/panel/rezervasyonlar/filitreli', 'rezervasyonController@indexFilitreli');
Route::get('/panel/rezervasyonlar/filitreliAd', 'rezervasyonController@indexFilitreliAd');
Route::get('/panel/rezervasyonlar/yarin', 'rezervasyonController@indexYarin');
Route::get('/panel/rezervasyon/ekle', 'rezervasyonController@ekleForm');
Route::get('/panel/rezervasyon/ekle/asb/{id}', 'rezervasyonController@ekleFormAsb');
Route::post('/panel/rezervasyon/ekle', 'rezervasyonController@eklePost');
Route::get('/panel/rezervasyon/duzenle/{id}', 'rezervasyonController@duzenleForm');
Route::post('/panel/rezervasyon/duzenle/{id}', 'rezervasyonController@duzenlePost');
Route::get('/panel/rezervasyon/sil/{id}', 'rezervasyonController@sil');
Route::get('/panel/rezervasyon/detay/{id}', 'rezervasyonController@detay');

Route::get('/panel/caridetaylari', 'cariController@index');
Route::get('/panel/caridetaylari/filitreli', 'cariController@indexFilitreli');
Route::get('/panel/caridetaylari/{id}', 'cariController@indexID');
Route::get('/panel/cari/gonder/{id}', 'cariController@ekleForm');
Route::post('/panel/cari/gonder/{id}', 'cariController@eklePost');
//Route::get('/panel/cari/duzenle/{id}', 'cariController@duzenleForm');
//Route::post('/panel/cari/duzenle/{id}', 'cariController@duzenlePost');
Route::get('/panel/cari/sil/{id}', 'cariController@sil');
Route::get('/panel/cari/excel/', 'cariController@excelOut');
Route::get('/panel/cari/excel/{id}', 'cariController@excelOutID');

