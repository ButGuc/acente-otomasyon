<?php

namespace App\Http\Controllers;

class carilerOdemeler {

    //Öncelikle kendime not : bu sınıfın yeri bura değil. niye üşeniyorsun üşenme.
    //Bu sınıfı cariler ve ödemeler tablosunu birleştirmekte kullanıyorum.
    //Bunun nedeni ise bldae üzerinede output alırken ortalığı karıştırmamak.

    public $id;
    public $rez;
    public $type;
    public $odeme;
    public $tarih;
    public $tur;
    public $y;
    public $c;
    public $b;
    public $u;
    public $borc;
    public $alacak;
    public $bakiye;
    public $aciklama;
    public $tutar;

    public function __construct()
    {
        $this->id = 0;
        $this->rez = 0;
        $this->type = 0;
        $this->odeme = 0;
        $this->tarih = null;
        $this->tur = null;
        $this->y = 0;
        $this->c = 0;
        $this->b = 0;
        $this->u = 0;
        $this->borc = 0;
        $this->alacak = 0;
        $this->bakiye = 0;
        $this->aciklama = null;
        $this->tutar = null;
    }

    public static function odemeEkle($id,$odeme, $tarih, $bakiye, $aciklama, $tutar) {
        $obj = new carilerOdemeler();

        $obj->ıd = $id;
        $obj->type = 0;
        $obj->odeme = $odeme;
        $obj->tarih = $tarih;
        $obj->bakiye = $bakiye;
        $obj->aciklama = $aciklama;
        $obj->tutar = $tutar;

        return $obj;
    }

    public static function cariEkle($rez_id, $id, $tarih, $tur, $y, $c, $b, $u, $borc, $alacak, $bakiye) {
        $obj = new carilerOdemeler();

        $obj->rez =$rez_id;
        $obj->ıd = $id;
        $obj->tarih = $tarih;
        $obj->type = 1;
        $obj->tur = $tur;
        $obj->y = $y;
        $obj->c = $c;
        $obj->b = $b;
        $obj->u = $u;
        $obj->borc = $borc;
        $obj->alacak = $alacak;
        $obj->bakiye = $bakiye;

        return $obj;
    }
}
