<?php

namespace App\Http\Controllers;

use App\Asb;
use App\Oteller;
use App\Rehberler;
use App\Rezervasyonlar;
use App\Turlar;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\App;

class rezervasyonController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        //Bugün olanm rezervasyonlara göre filitreler.
        //$pages = Rezervasyonlar::where('rezervasyon_tarihi',date("Y-m-d",strtotime("now")))->paginate(10);

        $pages = Rezervasyonlar::orderBy('rezervasyon_tarihi', 'ASC')->paginate(10);
        $tur = Turlar::all();
        $rehber = Rehberler::all();

        return view('panel.rezervasyon.index', compact('tur','rehber'))->withPosts($pages);
    }

    public function indexYarin() {
        $pages = Rezervasyonlar::where('rezervasyon_tarihi',date("Y-m-d",strtotime("tomorrow")))->paginate(10);
        $tur = Turlar::all();
        $rehber = Rehberler::all();

        return view('panel.rezervasyon.index', compact('tur','rehber'))->withPosts($pages);
    }

    public function indexFilitreli(Requests\rezervasyonFilitreleRequest $request) {
        $pages = Rezervasyonlar::orderBy('rezervasyon_tarihi', 'ASC')
            ->whereBetween('rezervasyon_tarihi',  array($request->input('tarih1'), $request->input('tarih2')))
            ->where('tur_id', $request->input('tur'))
            ->where('rehber_id', $request->input('rehber'))
            ->paginate(1);

        $rezervasyon = Rezervasyonlar::all();
        $tur = Turlar::all();
        $rehber = Rehberler::all();
        $request->flash();

        return view('panel.rezervasyon.index', compact('rezervasyon','tur','rehber','request'))->withPosts($pages);
    }

    public function indexFilitreliAd(Requests\rezervasyonFilitreleAdRequest $request) {
        $pages = Rezervasyonlar::orderBy('rezervasyon_tarihi', 'ASC')
            ->where('rezervasyon_adi', "like" ,  "%".$request->input('adi')."%")
            ->paginate(1);

        $rezervasyon = Rezervasyonlar::all();
        $tur = Turlar::all();
        $rehber = Rehberler::all();
        $request->flash();

        return view('panel.rezervasyon.index', compact('rezervasyon','tur','rehber','request'))->withPosts($pages);
    }

    public function detay($id) {
        $rezervasyon = Rezervasyonlar::find($id);

        return view('panel.rezervasyon.detay',compact('rezervasyon'));
    }

    public function sil($id) {
        $rezervasyon = Rezervasyonlar::find($id);
        $rezervasyon->delete();

        return redirect('/panel/rezervasyonlar/')->with('status','Rezervasyon Silindi');
    }

    public function ekleForm() {
        $tur = Turlar::all();
        $otel = Oteller::all();
        $rehber = Rehberler::all();

        return view('panel.rezervasyon.ekle',compact('tur','otel','rehber'));
    }

    public function ekleFormAsb($id) {
        $asb = Asb::where('tur_id', $id)->get();

        return view('panel.rezervasyon.asb',compact('asb'));
    }

    public function eklePost(Requests\rezervasyonEkleRequest $request) {
        $newrezervasyon = new Rezervasyonlar();

        $newrezervasyon->rezervasyon_tarihi = $request->get('tarih');
        $newrezervasyon->rezervasyon_yetiskin_pax = $request->get('yetiskinPax');
        $newrezervasyon->rezervasyon_cocuk_pax = $request->get('cocukPac');
        $newrezervasyon->rezervasyon_bebek_pax = $request->get('bebekPax');
        $newrezervasyon->rezervasyon_ucret_pax = $request->get('ucretPax');
        $newrezervasyon->rezervasyon_adi = $request->get('isim');
        $newrezervasyon->rezervasyon_no = $request->get('biletNo');
        $newrezervasyon->rezervasyon_oda_no = $request->get('odaNo');
        $newrezervasyon->rezervasyon_tel = $request->get('telNo');
        $newrezervasyon->rezervasyon_toplam_satis = $request->get('toplamSatis');
        $newrezervasyon->rezervasyon_toplam_satis_doviz = $request->get('toplamSatisD');
        $newrezervasyon->rezervasyon_kapora = $request->get('kapora');
        $newrezervasyon->rezervasyon_kapora_doviz = $request->get('kaporaD');
        $newrezervasyon->rezervasyon_rest = $request->get('rest');
        $newrezervasyon->rezervasyon_rest_doviz = $request->get('restD');
        $newrezervasyon->tur_id = $request->get('tur');
        $newrezervasyon->otel_id = $request->get('otel');
        $newrezervasyon->asb_id = $request->get('asb');
        $newrezervasyon->rehber_id = $request->get('rehber');

        $newrezervasyon->save();

        return redirect('panel/rezervasyon/ekle')->with('status','Rezervasyon Eklendi');
    }

    public function duzenleForm($id) {
        $rezervasyon = Rezervasyonlar::find($id);
        $tur = Turlar::all();
        $asb = Asb::where('tur_id', $rezervasyon->tur_id)->get();
        $otel = Oteller::all();
        $rehber = Rehberler::all();

        return view('panel.rezervasyon.duzenle', compact('rezervasyon','tur','otel','rehber','asb'));
    }

    public function duzenlePost(Requests\rezervasyonEkleRequest $request,$id) {
        $rezervasyon = Rezervasyonlar::find($id);

        $rezervasyon->rezervasyon_tarihi = $request->get('tarih');
        $rezervasyon->rezervasyon_yetiskin_pax = $request->get('yetiskinPax');
        $rezervasyon->rezervasyon_cocuk_pax = $request->get('cocukPac');
        $rezervasyon->rezervasyon_bebek_pax = $request->get('bebekPax');
        $rezervasyon->rezervasyon_ucret_pax = $request->get('ucretPax');
        $rezervasyon->rezervasyon_adi = $request->get('isim');
        $rezervasyon->rezervasyon_no = $request->get('biletNo');
        $rezervasyon->rezervasyon_oda_no = $request->get('odaNo');
        $rezervasyon->rezervasyon_tel = $request->get('telNo');
        $rezervasyon->rezervasyon_toplam_satis = $request->get('toplamSatis');
        $rezervasyon->rezervasyon_toplam_satis_doviz = $request->get('toplamSatisD');
        $rezervasyon->rezervasyon_kapora = $request->get('kapora');
        $rezervasyon->rezervasyon_kapora_doviz = $request->get('kaporaD');
        $rezervasyon->rezervasyon_rest = $request->get('rest');
        $rezervasyon->rezervasyon_rest_doviz = $request->get('restD');
        $rezervasyon->tur_id = $request->get('tur');
        $rezervasyon->otel_id = $request->get('otel');
        $rezervasyon->asb_id = $request->get('asb');
        $rezervasyon->rehber_id = $request->get('rehber');

        $rezervasyon->save();

        return redirect('panel/rezervasyon/duzenle/'.$id)->with('status','Rezervasyon Düzenlendi');
    }
}
