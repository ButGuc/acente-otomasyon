<?php

namespace App\Http\Controllers;

use App\Turlar;
use Illuminate\Http\Request;
use App\Http\Requests;

class turController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $tur = Turlar::all();

        return view('panel.tur.index', compact('tur'));
    }

    public function sil($id) {
        $tur = Turlar::find($id);
        $tur->delete();

        return redirect('/panel/turlar/')->with('status','Tur Silindi');
    }

    public function ekleForm() {
        return view('panel.tur.ekle');
    }

    public function eklePost(Requests\turEkleRequest $request) {
        $newtur = new Turlar();
        $newtur->tur_adi = $request->get('turadi');
        $newtur->save();

        return redirect('panel/tur/ekle')->with('status','Tur Eklendi');
    }

    public function duzenleForm($id) {
        $tur = Turlar::find($id);

        return view('panel.tur.duzenle', compact('tur'));
    }

    public function duzenlePost(Requests\turEkleRequest $request,$id) {
        $tur = Turlar::find($id);
        $tur->tur_adi = $request->get('turadi');
        $tur->save();

        return redirect('panel/tur/duzenle/'.$id)->with('status','Tur Düzenlendi');
    }
}
