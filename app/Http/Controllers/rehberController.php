<?php

namespace App\Http\Controllers;

use App\Rehberler;
use Illuminate\Http\Request;
use App\Http\Requests;

class rehberController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $rehber = Rehberler::all();

        return view('panel.rehber.index', compact('rehber'));
    }

    public function sil($id) {
        $rehber = Rehberler::find($id);
        $rehber->delete();

        return redirect('/panel/rehberler/')->with('status','Rehber Silindi');
    }

    public function ekleForm() {
        return view('panel.rehber.ekle');
    }

    public function eklePost(Requests\rehberEkleRequest $request) {
        $newrehber = new Rehberler();
        $newrehber->rehber_adi = $request->get('rehberadi');
        $newrehber->save();

        return redirect('panel/rehber/ekle')->with('status','Rehber Eklendi');
    }

    public function duzenleForm($id) {
        $rehber = Rehberler::find($id);

        return view('panel.rehber.duzenle', compact('rehber'));
    }

    public function duzenlePost(Requests\rehberEkleRequest $request,$id) {
        $rehber = Rehberler::find($id);
        $rehber->rehber_adi = $request->get('rehberadi');
        $rehber->save();

        return redirect('panel/rehber/duzenle/'.$id)->with('status','Rehber Düzenlendi');
    }
}
