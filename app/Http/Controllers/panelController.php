<?php

namespace App\Http\Controllers;

use App\cariOdemeler;
use App\Turlar;
use Teknomavi\Tcmb\Doviz;
use App\Http\Requests;

class panelController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        return view('panel.panel');
    }

    public function sistemayarlari() {
        return view('panel.sistemayarlari');
    }

    public function cariler() {
        return view('panel.cariler');
    }

    public function istatistikler() {
        $output = array();

        return view('panel.istatistik',compact('output'));
    }

    public function istatistiklerFilitreli(Requests\istatistikRequest $request) {
        $doviz = new Doviz(); $dovizler = array('USD' => null, 'EUR' => null, 'GBP' => null);
        $dovizler['USD'] = $doviz->kurAlis("USD", Doviz::TYPE_EFEKTIFALIS);
        $dovizler['EUR'] = $doviz->kurAlis("EUR", Doviz::TYPE_EFEKTIFALIS);
        $dovizler['GBP'] = $doviz->kurAlis("GBP", Doviz::TYPE_EFEKTIFALIS);

        $output = array();

        $turlar = Turlar::with(['Rezervasyonlar' => function ($query) use ($request) {
            $query->whereBetween('rezervasyon_tarihi', array($request->input('tarih1'),$request->input('tarih2')));
        }])->get();

        foreach ($turlar as $item) {
            $try = 0; $usd = 0; $eur = 0; $gbp = 0;
            if (count($item->Rezervasyonlar) >= 1) {
                foreach ($item->Rezervasyonlar as $item2) {
                    if (!empty($item2->Cariler)) {
                        if ($item2->rezervasyon_toplam_satis_doviz == $item2->Cariler->Cari_Hesaplar->ch_doviz) {
                            switch ($item2->rezervasyon_toplam_satis_doviz) {
                                case "TRY": $try += ($item2->rezervasyon_toplam_satis - $item2->Cariler->cari_alacak); break;
                                case "USD": $usd += ($item2->rezervasyon_toplam_satis - $item2->Cariler->cari_alacak); break;
                                case "EUR": $eur += ($item2->rezervasyon_toplam_satis - $item2->Cariler->cari_alacak); break;
                                case "GBP": $gbp += ($item2->rezervasyon_toplam_satis - $item2->Cariler->cari_alacak); break;
                            }
                        } else {
                            $doviz = $item2->Cariler->Cari_Hesaplar->ch_doviz;
                            switch ($item2->rezervasyon_toplam_satis_doviz) {
                                case "TRY": $try += ($item2->rezervasyon_toplam_satis - ($dovizler[$doviz] * $item2->Cariler->cari_alacak)); break;
                                case "USD": $usd += ($item2->rezervasyon_toplam_satis - (($item2->Cariler->cari_alacak * $dovizler['USD']) / $dovizler[$doviz])); break;
                                case "EUR": $eur += ($item2->rezervasyon_toplam_satis - (($item2->Cariler->cari_alacak * $dovizler['EUR']) / $dovizler[$doviz])); break;
                                case "GBP": $gbp += ($item2->rezervasyon_toplam_satis - (($item2->Cariler->cari_alacak * $dovizler['GBP']) / $dovizler[$doviz])); break;
                            }
                        }
                    }
                }
                array_push($output, array("tur_adi" => $item->tur_adi,
                    "try" => $try,
                    "usd" => $usd,
                    "eur" => $eur,
                    "gbp" => $gbp
                ));
            }
        }

        $request->flash();
        return view('panel.istatistik',compact('output'));
    }

    public function dovizDetay() {
        $doviz = new Doviz(); $dovizler = array('USD' => null, 'EUR' => null, 'GBP' => null);

        $dovizler['USD'] = $doviz->kurAlis("USD", Doviz::TYPE_EFEKTIFALIS);
        $dovizler['EUR'] = $doviz->kurAlis("EUR", Doviz::TYPE_EFEKTIFALIS);
        $dovizler['GBP'] = $doviz->kurAlis("GBP", Doviz::TYPE_EFEKTIFALIS);

        return view('panel.dovizDetay',compact('dovizler'));
    }
}

