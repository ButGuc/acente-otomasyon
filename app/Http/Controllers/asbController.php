<?php

namespace App\Http\Controllers;

use App\Asb;
use App\Turlar;
use Illuminate\Http\Request;

use App\Http\Requests;

class asbController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $asb = Asb::all();

        return view('panel.asb.index', compact('asb','tur'));
    }

    public function sil($id) {
        $asb = Asb::find($id);
        $asb->delete();

        return redirect('/panel/asbler/')->with('status','Alınış Saati Silindi');
    }

    public function ekleForm() {
        $tur = Turlar::all();

        return view('panel.asb.ekle', compact('tur'));
    }

    public function eklePost(Requests\asbEkleRequest $request) {
        $newasb = new Asb();

        $newasb->asb_value = $request->get('value');
        $newasb->tur_id = $request->get('turid');

        $newasb->save();

        return redirect('panel/asb/ekle')->with('status','Alınış Saati Eklendi');
    }

    public function duzenleForm($id) {
        $asb = Asb::find($id);
        $tur = Turlar::all();

        return view('panel.asb.duzenle', compact('asb','tur'));
    }

    public function duzenlePost(Requests\asbEkleRequest $request,$id) {
        $asb = Asb::find($id);

        $asb->asb_value = $request->get('value');
        $asb->tur_id = $request->get('turid');

        $asb->save();

        return redirect('panel/asb/duzenle/'.$id)->with('status','Alınış Saati Düzenlendi');
    }
}
