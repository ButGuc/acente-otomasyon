<?php

namespace App\Http\Controllers;

use App\Oteller;
use PhpParser\Node\Expr\AssignOp\Concat;
use Illuminate\Http\Request;
use App\Http\Requests;

class otelController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $otel = Oteller::all();

        return view('panel.otel.index', compact('otel'));
    }

    public function sil($id) {
        $otel = Oteller::find($id);
        $otel->delete();

        return redirect('/panel/oteller/')->with('status','Otel Silindi');
    }

    public function ekleForm() {
        return view('panel.otel.ekle');
    }

    public function eklePost(Requests\otelEkleRequest $request) {
        $newOtel = new Oteller();
        $newOtel->otel_adi = $request->get('oteladi');
        $newOtel->save();

        return redirect('panel/otel/ekle')->with('status','Otel Eklendi');
    }

    public function duzenleForm($id) {
        $otel = Oteller::find($id);

        return view('panel.otel.duzenle', compact('otel'));
    }

    public function duzenlePost(Requests\otelEkleRequest $request,$id) {
        $otel = Oteller::find($id);
        $otel->otel_adi = $request->get('oteladi');
        $otel->save();

        return redirect('panel/otel/duzenle/'.$id)->with('status','Otel Düzenlendi');
    }
}