<?php

namespace App\Http\Controllers;

use App\Cari_Hesaplar;
use Illuminate\Http\Request;
use App\Http\Requests;

class cariHesapController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $cari_hesap = Cari_Hesaplar::all();

        return view('panel.cari_hesap.index', compact('cari_hesap'));
    }

    public function sil($id) {
        $cari_hesap = Cari_Hesaplar::find($id);
        $cari_hesap->delete();

        return redirect('/panel/cari/hesaplar/')->with('status','Cari Hesap Silindi');
    }

    public function ekleForm() {
        return view('panel.cari_hesap.ekle');
    }

    public function eklePost(Requests\cariHesapEkleRequest $request) {
        $newcari_hesap = new Cari_Hesaplar();

        $newcari_hesap->ch_adi = $request->get('hesapadi');
        $newcari_hesap->ch_doviz = $request->get('dovizturu');

        $newcari_hesap->save();

        return redirect('panel/cari/hesap/ekle')->with('status','Cari Hesap Eklendi');
    }

    public function duzenleForm($id) {
        $cari_hesap = Cari_Hesaplar::find($id);

        return view('panel.cari_hesap.duzenle', compact('cari_hesap'));
    }

    public function duzenlePost(Requests\cariHesapDuzenleRequest $request,$id) {
        $cari_hesap = Cari_Hesaplar::find($id);

        $cari_hesap->ch_adi = $request->get('hesapadi');

        $cari_hesap->save();

        return redirect('panel/cari/hesap/duzenle/'.$id)->with('status','Cari Hesap Düzenlendi');
    }
}
