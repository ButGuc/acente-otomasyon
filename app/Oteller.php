<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oteller extends Model
{
    protected $primaryKey = 'otel_id';
    protected $table = 'oteller';

    public $fillable = [
        'otel_adi'
    ];

    public function Rezervasyonlar() {
        return $this->hasMany('App\Rezervasyonlar','otel_id');
    }
}
