<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rezervasyonlar extends Model
{
    protected $primaryKey = 'rezervasyonlar_id';
    protected $table = 'rezervasyonlar';

    public $fillable = [
        'rezervasyon_no','rezervasyon_tarihi','rezervasyon_adi','rezervasyon_tel','rezervasyon_tel_no'
        ,'rezervasyon_toplam_satis','rezervasyon_toplam_satis_doviz','rezervasyon_komisyon','rezervasyon_komisyon_doviz','rezervasyon_rest','rezervasyon_rest_doviz'
        ,'rezervasyon_yetiskin_pax','rezervasyon_cocuk_pax','rezervasyon_bebek_pax','rezervasyon_ucret_pax'
        ,'otel_id','tur_id','rehber_id','asb_id'
    ];

    public function Turlar() {
        return $this->belongsTo('App\Turlar', 'tur_id');
    }

    public function Oteller() {
        return $this->belongsTo('App\Oteller', 'otel_id');
    }

    public function Rehberler() {
        return $this->belongsTo('App\Rehberler', 'rehber_id');
    }

    public function Asb() {
        return $this->belongsTo('App\Asb', 'asb_id');
    }

    public function Cariler() {
        return $this->hasOne('App\Cariler','rezervasyonlar_id');
    }
}
