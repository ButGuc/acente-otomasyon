<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rehberler extends Model
{
    protected $primaryKey = 'rehber_id';
    protected $table = 'rehberler';

    public $fillable = [
        'rehber_adi'
    ];

    public function Rezervasyonlar() {
        return $this->hasMany('App\Rezervasyonlar','rehber_id');
    }
}
