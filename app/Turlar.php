<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turlar extends Model
{
    protected $primaryKey = 'tur_id';
    protected $table = 'turlar';

    public $fillable = [
        'tur_adi'
    ];

    public function Asb() {
        return $this->hasMany('App\Asb','tur_id');
    }

    public function Rezervasyonlar() {
        return $this->hasMany('App\Rezervasyonlar','tur_id');
    }

    public function Cariler() {
        return $this->hasMany('App\Cariler','tur_id');
    }
}
