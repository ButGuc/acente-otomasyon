<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asb extends Model
{
    protected $primaryKey = 'asb_id';
    protected $table = 'asb';

    public $fillable = [
        'asb_value','tur_id'
    ];

    public function Turlar() {
        return $this->belongsTo('App\Turlar', 'tur_id');
    }

    public function Rezervasyonlar() {
        return $this->hasMany('App\Rezervasyonlar','asb_id');
    }
}
