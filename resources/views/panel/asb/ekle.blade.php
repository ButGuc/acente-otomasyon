@extends('layouts.app')
@section('content')
    <form action="/panel/asb/ekle" method="post">
        {{ csrf_field() }}

        @foreach ($errors->all() as $error)
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                </div>
            </div>
        @endforeach
        @if (session('status'))
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-info">
                        {{session('status')}}
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-sm-12">
                <h2>Alınış Saati Ekle</h2>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="form-group row">
                    <label for="value" class="col-4 col-form-label">Veri</label>
                    <div class="col-8">
                        <input class="form-control" type="text" value="" id="value" name="value">
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-6">
                <div class="form-group row">
                    <label for="turid" class="col-4 col-form-label">Tur Seç</label>
                    <div class="col-8">
                        <select class="form-control" id="turid" name="turid">
                            @forelse ($tur as $o)
                                <option value="{{ $o->tur_id }}">{{ $o->tur_adi }}</option>
                            @empty
                                <option>Lütfen Bir Tur Ekleyin !!</option>
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="row">
                    <div class="col-12" style="margin-top: -1rem">
                        <hr>
                    </div>
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary">Alınış Saati Ekle</button>
                        <input type="reset" class="btn btn-warning" value="Sıfırla">
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection