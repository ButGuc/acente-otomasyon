@extends('layouts.app')
@section('content')
    @if (session('status'))
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-info">
                    {{session('status')}}
                </div>
            </div>
        </div>
    @endif
    @foreach ($errors->all() as $error)
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-danger">
                    {{ $error }}
                </div>
            </div>
        </div>
    @endforeach
    <div class="row">
        <div class="col-12">
            <h3>Turlara Göre Gelir İstatistiği</h3>
            <hr>
        </div>
    </div>
    <form action="{{url('/panel/istatistikler/filitreli')}}" method="get">
        {{csrf_field()}}
        <div class="row">
            <div class="col-12">
                <div class="form-group row">
                    <label for="tarih" class="col-2 col-form-label">Tarih</label>
                    <div class="col-4">
                        <input class="form-control" type="date" value="{{ old('tarih1') }}" id="tarih1" name="tarih1">
                    </div>
                    <div class="col-2">
                        <label for="tarih2" class="col-2 col-form-label"> Arası </label>
                    </div>
                    <div class="col-4">
                        <input class="form-control" type="date" value="{{ old('tarih2') }}" id="tarih2" name="tarih2">
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12">
                <div class="row">
                    <div class="col-12" style="margin-top: -1rem">
                        <hr>
                    </div>
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-sm btn-primary">İstatistik Filitrele</button>
                        <input type="reset" class="btn btn-sm btn-warning" value="Sıfırla">
                    </div>
                </div>
            </div>
            <div class="col-12"><hr></div>
        </div>
    </form>
    <div class="row">
        <div class="col-12">
            <div class="table-responsive">
                <table class="table table-bordered ">
                    <thead>
                    <tr>
                        <th class="text-center">Tur</th>
                        <th class="text-center">TRY</th>
                        <th class="text-center">USD</th>
                        <th class="text-center">EUR</th>
                        <th class="text-center">GBP</th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse ($output as $o)
                        <tr>
                            <td class="text-center">{{$o['tur_adi']}}</td>
                            <td class="text-center">{{$o['try']}}</td>
                            <td class="text-center">{{$o['usd']}}</td>
                            <td class="text-center">{{$o['eur']}}</td>
                            <td class="text-center">{{$o['gbp']}}</td>
                        </tr>
                    @empty
                        <p>Boş</p>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection