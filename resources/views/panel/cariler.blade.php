@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-12">
        <h3>Cariler</h3>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-md-6">
        <div class="row">
            <div class="col-6">
                <h4>Cariler</h4>
            </div>
            <div class="col-6">
                <a class="btn btn-sm btn-block btn-outline-info" href="{{ url('/panel/caridetaylari') }}">Cari Detayları</a>
            </div>
        </div>
        <hr>
    </div>
    <div class="col-sm-12 col-md-6">
        <div class="row">
            <div class="col-6">
                <h4>Cari Hesaplar</h4>
            </div>
            <div class="col-3">
                <a class="btn btn-sm btn-block btn-outline-success" href="{{ url('/panel/cari/hesap/ekle') }}">Ekle</a>
            </div>
            <div class="col-3">
                <a class="btn btn-sm btn-block btn-outline-warning" href="{{ url('/panel/cari/hesaplar') }}">Düzenle</a>
            </div>
        </div>
        <hr>
    </div>
    <div class="col-sm-12 col-md-6">
        <div class="row">
            <div class="col-6">
                <h4>Ödemeler</h4>
            </div>
            <div class="col-3">
                <a class="btn btn-sm btn-block btn-outline-success" href="{{ url('/panel/odeme/ekle') }}">Yap</a>
            </div>
            <div class="col-3">
                <a class="btn btn-sm btn-block btn-outline-info" href="{{ url('/panel/odemeler') }}">Detaylar</a>
            </div>
        </div>
        <hr>
    </div>
</div>
@endsection