@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-12">
        <h3>Döviz Kurları</h3>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-md-4">
        <div class="row">
            <div class="col-2">
                <i class="fa fa-usd" aria-hidden="true"></i>
            </div>
            <div class="col-4">
                <p>USD > TRY</p>
            </div>
            <div class="col-6">
                <p>{{ $dovizler['USD'] }}</p>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="row">
            <div class="col-2">
                <i class="fa fa-eur" aria-hidden="true"></i>
            </div>
            <div class="col-4">
                <p>EUR > TRY</p>
            </div>
            <div class="col-6">
                <p>{{ $dovizler['EUR'] }}</p>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="row">
            <div class="col-2">
                <i class="fa fa-gbp" aria-hidden="true"></i>
            </div>
            <div class="col-4">
                <p>GBP > TRY</p>
            </div>
            <div class="col-6">
                <p>{{ $dovizler['GBP'] }}</p>
            </div>
        </div>
    </div>
</div>
@endsection