@extends('layouts.app')
@section('content')
    <form action="/panel/cari/hesap/duzenle/{{ $cari_hesap->ch_id }}" method="post">
        {{ csrf_field() }}

        @foreach ($errors->all() as $error)
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                </div>
            </div>
        @endforeach
        @if (session('status'))
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-info">
                        {{session('status')}}
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-sm-12">
                <h2>Cari Hesap Düzenle</h2>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="form-group row">
                    <label for="hesapadi" class="col-4 col-form-label">Cari Hesap Adı</label>
                    <div class="col-8">
                        <input class="form-control" type="text" value="{{ $cari_hesap->ch_adi }}" id="hesapadi" name="hesapadi">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="row">
                    <div class="col-12" style="margin-top: -1rem">
                        <hr>
                    </div>
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary">Cari Hesap Düzenle</button>
                        <input type="reset" class="btn btn-warning" value="Sıfırla">
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection