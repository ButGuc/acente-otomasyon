@extends('layouts.app')
@section('content')
@if (session('status'))
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-info">
                {{session('status')}}
            </div>
        </div>
    </div>
@endif

<div class="row">
    <div class="col-12">
        <h3>Cari Hesaplar</h3>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="text-center">Hesap Adı</th>
                <th class="text-center">Döviz</th>
                <th class="text-center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
                <th class="text-center"><i class="fa fa-trash-o" aria-hidden="true"></i></th>
            </tr>
            </thead>
            <tbody>
                @forelse ($cari_hesap as $o)
                    <tr>
                        <td>{{$o->ch_adi}}</td>
                        <td>{{$o->ch_doviz}}</td>
                        <td class="text-center"><a class="btn btn-sm btn-outline-warning" href="{{ url('/panel/cari/hesap/duzenle/'.$o->ch_id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                        <td class="text-center"><a class="btn btn-sm btn-outline-danger" href="{{ url('/panel/cari/hesap/sil/'.$o->ch_id) }}"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                    </tr>
                @empty
                    <p>Boş</p>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection