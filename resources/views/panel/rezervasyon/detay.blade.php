@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-12">
        <h3>Rezervasyon Detayı ({{ $rezervasyon->rezervasyonlar_id }})</h3>
        <hr>
    </div>
    <div class="col-12">
        <p><strong>Tarihi : </strong> {{ $rezervasyon->rezervasyon_tarihi }}</p>
        <p><strong>Adı : </strong> {{ $rezervasyon->rezervasyon_adi }}</p>
        <p><strong>Bilet No : </strong> {{ $rezervasyon->rezervasyon_no }}</p>
        <p><strong>Tel : </strong> {{ $rezervasyon->rezervasyon_tel }}</p>
        <p><strong>Oda No : </strong> {{ $rezervasyon->rezervasyon_oda_no }}</p>
        <p><strong>Otel Adı : </strong> {{ $rezervasyon->Oteller->otel_adi }}</p>
        <p><strong>Alınış Saati : </strong> {{ $rezervasyon->Asb->asb_value }}</p>
        <p><strong>Paxs : </strong> Yetişkin : {{ $rezervasyon->rezervasyon_yetiskin_pax }} -
            Çocuk : {{ $rezervasyon->rezervasyon_cocuk_pax }} -
            Bebek : {{ $rezervasyon->rezervasyon_bebek_pax }} -
            Ücret : {{ $rezervasyon->rezervasyon_ucret_pax }} </p>
        <p><strong>Toplam Satış : </strong> {{ $rezervasyon->rezervasyon_toplam_satis." ".$rezervasyon->rezervasyon_toplam_satis_doviz }}</p>
        <p><strong>Kapora : </strong> {{ $rezervasyon->rezervasyon_kapora." ".$rezervasyon->rezervasyon_kapora_doviz }}</p>
        <p><strong>Rest : </strong> {{ $rezervasyon->rezervasyon_rest." ".$rezervasyon->rezervasyon_rest_doviz }}</p>
        <p><strong>Cari : </strong> {{ (empty($rezervasyon->Cariler)) ? "Gönderilmedi" : "Gönderildi (".$rezervasyon->Cariler->cari_id.")" }}</p>
    </div>
</div>
@endsection