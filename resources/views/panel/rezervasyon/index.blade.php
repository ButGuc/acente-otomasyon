@extends('layouts.app')
@section('content')
@if (session('status'))
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-info">
                {{session('status')}}
            </div>
        </div>
    </div>
@endif
@foreach ($errors->all() as $error)
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                {{ $error }}
            </div>
        </div>
    </div>
@endforeach
<div class="row">
    <div class="col-12">
        <h3>Rezervasyonlar</h3>
        <hr>
    </div>
</div>
<form action="{{url('/panel/rezervasyonlar/filitreliAd')}}" method="get">
    {{csrf_field()}}
    <input type="text" value="0" name="ftype" hidden>
    <div class="row">
        <div class="col-sm-8 col-md-6">
            <div class="form-group row">
                <label for="turAsbTrigger" class="col-4 col-form-label">Ad</label>
                <div class="col-8">
                    <input class="form-control" type="text"  name="adi" placeholder="Adı">
                </div>
            </div>
        </div>
        <div class="col-sm-4 col-md-6">
            <div class="form-group row">
                <div class="col-12">
                    <button type="submit" class="btn btn-primary">Ada Göre Ara</button>
                </div>
            </div>
        </div>
        <div class="col-12" style="margin-top: -1rem"><hr></div>
    </div>
</form>
<form action="{{url('/panel/rezervasyonlar/filitreli')}}" method="get">
    {{csrf_field()}}
    <input type="text" value="1" name="ftype" hidden>
    <div class="row">
        <div class="col-12">
            <div class="form-group row">
                <label for="tarih" class="col-2 col-form-label">Tarih</label>
                <div class="col-4">
                    <input class="form-control" type="date" value="{{old('tarih1')}}" id="tarih1" name="tarih1">
                </div>
                <div class="col-2">
                    <label for="tarih2" class="col-2 col-form-label"> Arası </label>
                </div>
                <div class="col-4">
                    <input class="form-control" type="date" value="{{old('tarih2')}}" id="tarih2" name="tarih2">
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="form-group row">
                <label for="turAsbTrigger" class="col-4 col-form-label">Tur</label>
                <div class="col-8">
                    <select class="form-control" id="turAsbTrigger" name="tur">
                        @forelse($tur as $item)
                            <option {{ (old('tur') == $item->tur_id) ? "selected" : "" }} value="{{$item->tur_id}}">{{$item->tur_adi}}</option>
                        @empty
                            <option>Boş</option>
                        @endforelse
                    </select>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="form-group row">
                <label for="rehber" class="col-4 col-form-label">Rehber</label>
                <div class="col-8">
                    <select class="form-control" id="rehber" name="rehber">
                        @forelse($rehber as $item)
                            <option {{ (old('rehber') == $item->rehber_id) ? "selected" : "" }} value="{{$item->rehber_id}}">{{$item->rehber_adi}}</option>
                        @empty
                            <option>Boş</option>
                        @endforelse
                    </select>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-12">
            <div class="row">
                <div class="col-12" style="margin-top: -1rem">
                    <hr>
                </div>
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-sm btn-primary">Rezervasyonları Filitrele</button>
                    <input type="reset" class="btn btn-sm btn-warning" value="Sıfırla">
                    <a class="btn btn-sm btn-info" href="{{ url('/panel/rezervasyonlar/yarin/')}}">Yarın Olan Rezervasyonlar</a>
                    <a class="btn btn-sm btn-success" href="{{ url('/panel/rezervasyon/ekle/')}}">Rezervasyon Ekle</a>
                </div>
            </div>
        </div>
        <div class="col-12"><hr></div>
    </div>
</form>
<div class="row">
    <div class="col-sm-12">
        <p>Cariye gönderilen rezervasyonlar üzerinde işlem yapılamaz. İşlem yapmanız için gönderilen cariyi silmeniz gerekir..</p>
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th class="text-center">Tarih</th>
                    <th class="text-center">Tur</th>
                    <th class="text-center">Y</th>
                    <th class="text-center">Ç</th>
                    <th class="text-center">B</th>
                    <th class="text-center">Ü</th>
                    <th class="text-center">Adı</th>
                    <th class="text-center">Otel</th>
                    <th class="text-center">Saat</th>
                    <th class="text-center">Satış</th>
                    <th class="text-center">Kapora</th>
                    <th class="text-center">Rest</th>
                    <th class="text-center"><i class="fa fa-question-circle" aria-hidden="true"></i></th>
                    <th class="text-center"><i class="fa fa-arrow-right" aria-hidden="true"></i></th>
                    <th class="text-center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
                    <th class="text-center"><i class="fa fa-trash-o" aria-hidden="true"></i></th>
                </tr>
                </thead>
                <tbody>
                @php $yetiskin = 0; $cocuk = 0; $bebek =0; $ucret = 0; @endphp
                @forelse ($posts as $o)
                    @php
                        $yetiskin += $o->rezervasyon_yetiskin_pax;
                        $cocuk += $o->rezervasyon_cocuk_pax;
                        $bebek += $o->rezervasyon_bebek_pax;
                        $ucret += $o->rezervasyon_ucret_pax;
                    @endphp
                    <tr class="{{ (empty($o->Cariler)) ? "" : "table-info" }}">
                        <td>{{$o->rezervasyonlar_id}}</td>
                        <td>{{$o->rezervasyon_tarihi}}</td>
                        <td>{{$o->Turlar->tur_adi}}</td>
                        <td>{{$o->rezervasyon_yetiskin_pax}}</td>
                        <td>{{$o->rezervasyon_cocuk_pax}}</td>
                        <td>{{$o->rezervasyon_bebek_pax}}</td>
                        <td>{{$o->rezervasyon_ucret_pax}}</td>
                        <td>{{$o->rezervasyon_adi}}</td>
                        <td>{{$o->Oteller->otel_adi}}</td>
                        <td>{{$o->Asb->asb_value}}</td>
                        <td>{{$o->rezervasyon_toplam_satis}} {{$o->rezervasyon_toplam_satis_doviz}}</td>
                        <td>{{$o->rezervasyon_kapora}} {{$o->rezervasyon_kapora_doviz}}</td>
                        <td>{{$o->rezervasyon_rest}} {{$o->rezervasyon_rest_doviz}}</td>
                        <td class="text-center"><a class="btn btn-sm btn-outline-info" href="{{ url('/panel/rezervasyon/detay/'.$o->rezervasyonlar_id) }}"><i class="fa fa-question-circle" aria-hidden="true"></i></a></td>
                        <td class="text-center"><a class="btn btn-sm btn-outline-primary {{ (empty($o->Cariler) ? "" : "disabled") }}" href="{{ url('/panel/cari/gonder/'.$o->rezervasyonlar_id) }}"><i class="fa fa-arrow-right" aria-hidden="true"></i></a></td>
                        <td class="text-center"><a class="btn btn-sm btn-outline-warning {{ (empty($o->Cariler) ? "" : "disabled") }}" href="{{ url('/panel/rezervasyon/duzenle/'.$o->rezervasyonlar_id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                        <td class="text-center"><a class="btn btn-sm btn-outline-danger {{ (empty($o->Cariler) ? "" : "disabled") }}" href="{{ url('/panel/rezervasyon/sil/'.$o->rezervasyonlar_id) }}"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                    </tr>
                @empty
                    <p>Boş</p>
                @endforelse
                    <tr>
                        <td colspan="3" class="text-center">Toplam Pax</td>
                        <td>{{ $yetiskin }}</td>
                        <td>{{ $cocuk }}</td>
                        <td>{{ $bebek }}</td>
                        <td>{{ $ucret }}</td>
                        <td colspan="10"></td>
                    </tr>
                </tbody>
            </table>
            <?php $link_limit = 7;
            if (isset($request)) {
                $tt = csrf_token();
                if ($request->input('ftype') == "1") {
                    $lastFiliterLink = "&_token=".$tt."&tarih1=".$request->input('tarih1')."&tarih2=".$request->input('tarih2')."&tur=".$request->input('tur')."&rehber=".$request->input('rehber')."&ftype=".$request->input('ftype')."";
                } else {
                    $lastFiliterLink = "&_token=".$tt."&adi=".$request->input('adi')."&ftype=".$request->input('ftype')."";
                }
            } ?>
            <nav>
                @if ($posts->lastPage() > 1)
                    <ul class="pagination">
                        <li class="page-item {{ ($posts->currentPage() == 1) ? ' disabled' : '' }}">
                            <a class="page-link" href="{{ $posts->url(1).(isset($lastFiliterLink) ? $lastFiliterLink : "" ) }}">İlk Sayfa</a>
                        </li>
                        @for ($i = 1; $i <= $posts->lastPage(); $i++)
                            <?php
                            $half_total_links = floor($link_limit / 2);
                            $from = $posts->currentPage() - $half_total_links;
                            $to = $posts->currentPage() + $half_total_links;
                            if ($posts->currentPage() < $half_total_links) {
                                $to += $half_total_links - $posts->currentPage();
                            }
                            if ($posts->lastPage() - $posts->currentPage() < $half_total_links) {
                                $from -= $half_total_links - ($posts->lastPage() - $posts->currentPage()) - 1;
                            }
                            ?>
                            @if ($from < $i && $i < $to)
                                <li class="page-item {{ ($posts->currentPage() == $i) ? ' active' : '' }}">
                                    <a class="page-link" href="{{ $posts->url($i).(isset($lastFiliterLink) ? $lastFiliterLink : "" ) }}">{{ $i }}</a>
                                </li>
                            @endif
                        @endfor
                        <li class="page-item {{ ($posts->currentPage() == $posts->lastPage()) ? ' disabled' : '' }}">
                            <a class="page-link" href="{{ $posts->url($posts->lastPage()).(isset($lastFiliterLink) ? $lastFiliterLink : "" ) }}">Son Sayfa</a>
                        </li>
                    </ul>
                @endif
            </nav>
        </div>
    </div>
</div>
@endsection