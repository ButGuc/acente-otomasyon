<select class="form-control" id="asb" name="asb">
    @forelse($asb as $item)
        <option value="{{$item->asb_id}}">{{$item->asb_value}}</option>
    @empty
        <option>Bir Alınış Saati Ekleyin</option>
    @endforelse
</select>