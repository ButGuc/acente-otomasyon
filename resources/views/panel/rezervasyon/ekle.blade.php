@extends('layouts.app')
@section('head')
    <script>
        jQuery.fn.filterByText = function(textbox, selectSingleMatch) {
            return this.each(function() {
                var select = this;
                var options = [];
                $(select).find('option').each(function() {
                    options.push({value: $(this).val(), text: $(this).text()});
                });
                $(select).data('options', options);
                $(textbox).bind('change keyup', function() {
                    var options = $(select).empty().data('options');
                    var search = $(this).val().trim();
                    var regex = new RegExp(search,"gi");

                    $.each(options, function(i) {
                        var option = options[i];
                        if(option.text.match(regex) !== null) {
                            $(select).append(
                                $('<option>').text(option.text).val(option.value)
                            );
                        }
                    });
                    if (selectSingleMatch === true && $(select).children().length === 1) {
                        $(select).children().get(0).selected = true;
                    }
                });
            });
        };

        $(function() {
            $('#select').filterByText($('#textbox'), false);
            $("select option").click(function(){
                alert(1);
            });
        });

        $(document).ready(function(){
            $('#turAsbTrigger').on('change', function() {
                var value = $(this).val();
                $.ajax({url: "{{ url('/panel/rezervasyon/ekle/asb') }}" + "/" + value, success: function(result){
                    $("#asbListDiv").html(result);
                }});
            });
        });
    </script>
@endsection
@section('content')
    <form action="/panel/rezervasyon/ekle" method="post">
        {{ csrf_field() }}

        @foreach ($errors->all() as $error)
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                </div>
            </div>
        @endforeach
        @if (session('status'))
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-info">
                        {{session('status')}}
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-sm-12">
                <h2>Rezervasyon Ekle</h2>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="form-group row">
                    <label for="tarih" class="col-4 col-form-label">Tarih</label>
                    <div class="col-8">
                        <input class="form-control" type="date" value="" id="tarih" name="tarih">
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="form-group row">
                    <label for="turAsbTrigger" class="col-4 col-form-label">Tur</label>
                    <div class="col-8">
                        <select class="form-control" id="turAsbTrigger" name="tur">
                            <option value="0">Lütfen Bir Tur Seçin</option>
                            @forelse($tur as $item)
                                <option value="{{$item->tur_id}}">{{$item->tur_adi}}</option>
                            @empty
                                <option>Boş</option>
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="form-group row">
                    <label for="yetiskinPax" class="col-6 col-form-label">Yetişkin Pax</label>
                    <div class="col-6">
                        <input class="form-control" type="text" value="0" id="yetiskinPax" name="yetiskinPax">
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group row">
                    <label for="cocukPac" class="col-6 col-form-label">Çocuk Pax</label>
                    <div class="col-6">
                        <input class="form-control" type="text" value="0" id="cocukPac" name="cocukPac">
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group row">
                    <label for="bebekPax" class="col-6 col-form-label">Bebek Pax</label>
                    <div class="col-6">
                        <input class="form-control" type="text" value="0" id="bebekPax" name="bebekPax">
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group row">
                    <label for="ucretPax" class="col-6 col-form-label">Ücret Pax</label>
                    <div class="col-6">
                        <input class="form-control" type="text" value="0" id="ucretPax" name="ucretPax">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="form-group row">
                    <label for="isim" class="col-4 col-form-label">Müşteri İsmi</label>
                    <div class="col-8">
                        <input class="form-control" type="text" value="" id="isim" name="isim">
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="form-group row">
                    <label for="biletNo" class="col-4 col-form-label">Bilet No</label>
                    <div class="col-8">
                        <input class="form-control" type="text" value="" id="biletNo" name="biletNo">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="form-group row">
                    <label for="textbox" class="col-4 col-form-label">Otel Seç</label>
                    <div class="col-8">
                        <input type="text" id="textbox" class="form-control" placeholder="Ara ...">
                    </div>
                    <div class="col-4"></div>
                    <div class="col-8">
                        <select class="form-control" id="select" name="otel" style="margin-top: -1px">
                            @forelse($otel as $item)
                                <option value="{{$item->otel_id}}">{{$item->otel_adi}}</option>
                            @empty
                                <option>Boş</option>
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="form-group row">
                    <label for="asb" class="col-4 col-form-label">Alınış Saati</label>
                    <div class="col-8" id="asbListDiv">
                        <select class="form-control" id="asb">
                            <option>Lütfen Bir Tur Seçin</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="form-group row">
                    <label for="odaNo" class="col-4 col-form-label">Oda No</label>
                    <div class="col-8">
                        <input class="form-control" type="text" value="" id="odaNo" name="odaNo">
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="form-group row">
                    <label for="telNo" class="col-4 col-form-label">Tel No</label>
                    <div class="col-8">
                        <input class="form-control" type="text" value="" id="telNo" name="telNo">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="form-group row">
                    <label for="rehber" class="col-4 col-form-label">Rehber</label>
                    <div class="col-8">
                        <select class="form-control" id="rehber" name="rehber">
                            @forelse($rehber as $item)
                                <option value="{{$item->rehber_id}}">{{$item->rehber_adi}}</option>
                            @empty
                                <option>Boş</option>
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12" style="margin-top: -1rem">
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <div class="form-group row">
                    <label for="toplamSatis" class="col-4 col-form-label">Toplam Satış</label>
                    <div class="col-4">
                        <input class="form-control" type="text" value="0" id="toplamSatis" name="toplamSatis">
                    </div>
                    <div class="col-4">
                        <select class="form-control" id="toplamSatisD" name="toplamSatisD">
                            <option value="TRY">TRY</option>
                            <option value="USD">USD</option>
                            <option value="EUR">EUR</option>
                            <option value="GBP">GBP</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="form-group row">
                    <label for="toplamSatis" class="col-4 col-form-label">Kapora</label>
                    <div class="col-4">
                        <input class="form-control" type="text" value="0" id="kapora" name="kapora">
                    </div>
                    <div class="col-4">
                        <select class="form-control" id="kaporaD" name="kaporaD">
                            <option value="TRY">TRY</option>
                            <option value="USD">USD</option>
                            <option value="EUR">EUR</option>
                            <option value="GBP">GBP</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="form-group row">
                    <label for="toplamSatis" class="col-4 col-form-label">Kalan Rest</label>
                    <div class="col-4">
                        <input class="form-control" type="text" value="0" id="rest" name="rest">
                    </div>
                    <div class="col-4">
                        <select class="form-control" id="restD" name="restD">
                            <option value="TRY">TRY</option>
                            <option value="USD">USD</option>
                            <option value="EUR">EUR</option>
                            <option value="GBP">GBP</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="row">
                    <div class="col-12" style="margin-top: -1rem">
                        <hr>
                    </div>
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary">Rezervasyon Ekle</button>
                        <input type="reset" class="btn btn-warning" value="Sıfırla">
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection