@extends('layouts.app')
@section('content')
    <form action="/panel/rezervasyon/duzenle/{{ $rezervasyon->rezervasyonlar_id }}" method="post">
        {{ csrf_field() }}

        @foreach ($errors->all() as $error)
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                </div>
            </div>
        @endforeach
        @if (session('status'))
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-info">
                        {{session('status')}}
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-sm-12">
                <h2>Rezervasyon Düzenle</h2>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="form-group row">
                    <label for="tarih" class="col-4 col-form-label">Tarih</label>
                    <div class="col-8">
                        <input class="form-control" type="date" value="{{ $rezervasyon->rezervasyon_tarihi }}" id="tarih" name="tarih">
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="form-group row">
                    <label for="turAsbTrigger" class="col-4 col-form-label">Tur</label>
                    <div class="col-8">
                        <select class="form-control" id="turAsbTrigger" name="tur">
                            <option value="0">Lütfen Bir Tur Seçin</option>
                            @forelse($tur as $item)
                                <option {{ ($item->tur_id == $rezervasyon->tur_id) ? "selected" : "" }} value="{{$item->tur_id}}">{{$item->tur_adi}}</option>
                            @empty
                                <option>Boş</option>
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="form-group row">
                    <label for="yetiskinPax" class="col-6 col-form-label">Yetişkin Pax</label>
                    <div class="col-6">
                        <input class="form-control" type="text" value="{{ $rezervasyon->rezervasyon_yetiskin_pax }}" id="yetiskinPax" name="yetiskinPax">
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group row">
                    <label for="cocukPac" class="col-6 col-form-label">Çocuk Pax</label>
                    <div class="col-6">
                        <input class="form-control" type="text" value="{{ $rezervasyon->rezervasyon_cocuk_pax }}" id="cocukPac" name="cocukPac">
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group row">
                    <label for="bebekPax" class="col-6 col-form-label">Bebek Pax</label>
                    <div class="col-6">
                        <input class="form-control" type="text" value="{{ $rezervasyon->rezervasyon_bebek_pax }}" id="bebekPax" name="bebekPax">
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group row">
                    <label for="ucretPax" class="col-6 col-form-label">Ücret Pax</label>
                    <div class="col-6">
                        <input class="form-control" type="text" value="{{ $rezervasyon->rezervasyon_ucret_pax }}" id="ucretPax" name="ucretPax">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="form-group row">
                    <label for="isim" class="col-4 col-form-label">Müşteri İsmi</label>
                    <div class="col-8">
                        <input class="form-control" type="text" value="{{ $rezervasyon->rezervasyon_adi }}" id="isim" name="isim">
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="form-group row">
                    <label for="biletNo" class="col-4 col-form-label">Bilet No</label>
                    <div class="col-8">
                        <input class="form-control" type="text" value="{{ $rezervasyon->rezervasyon_no }}" id="biletNo" name="biletNo">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="form-group row">
                    <label for="otel" class="col-4 col-form-label">Otel Seç</label>
                    <div class="col-8">
                        <select class="form-control" id="otel" name="otel">
                            @forelse($otel as $item)
                                <option {{ ($item->otel_id == $rezervasyon->otel_id) ? "selected" : "" }} value="{{$item->otel_id}}">{{$item->otel_adi}}</option>
                            @empty
                                <option>Boş</option>
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="form-group row">
                    <label for="asb" class="col-4 col-form-label">Alınış Saati</label>
                    <div class="col-8" id="asbListDiv">
                        <select class="form-control" id="asb" name="asb">
                            @forelse($asb as $item)
                                <option {{ ($item->asb_id == $rezervasyon->asb_id) ? "selected" : "" }} value="{{$item->asb_id}}">{{$item->asb_value}}</option>
                            @empty
                                <option>Boş</option>
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="form-group row">
                    <label for="odaNo" class="col-4 col-form-label">Oda No</label>
                    <div class="col-8">
                        <input class="form-control" type="text" value="{{ $rezervasyon->rezervasyon_oda_no }}" id="odaNo" name="odaNo">
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="form-group row">
                    <label for="telNo" class="col-4 col-form-label">Tel No</label>
                    <div class="col-8">
                        <input class="form-control" type="text" value="{{ $rezervasyon->rezervasyon_tel }}" id="telNo" name="telNo">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="form-group row">
                    <label for="rehber" class="col-4 col-form-label">Rehber</label>
                    <div class="col-8">
                        <select class="form-control" id="rehber" name="rehber">
                            @forelse($rehber as $item)
                                <option {{ ($item->otel_id == $rezervasyon->otel_id) ? "selected" : "" }} value="{{$item->rehber_id}}">{{$item->rehber_adi}}</option>
                            @empty
                                <option>Boş</option>
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12" style="margin-top: -1rem">
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <div class="form-group row">
                    <label for="toplamSatis" class="col-4 col-form-label">Toplam Satış</label>
                    <div class="col-4">
                        <input class="form-control" type="text" value="{{ $rezervasyon->rezervasyon_toplam_satis }}" id="toplamSatis" name="toplamSatis">
                    </div>
                    <div class="col-4">
                        <select class="form-control" id="toplamSatisD" name="toplamSatisD">
                            <option {{ ($rezervasyon->rezervasyon_toplam_satis_doviz == "TRY") ? "selected" : "" }} value="TRY">TRY</option>
                            <option {{ ($rezervasyon->rezervasyon_toplam_satis_doviz == "USD") ? "selected" : "" }} value="USD">USD</option>
                            <option {{ ($rezervasyon->rezervasyon_toplam_satis_doviz == "EUR") ? "selected" : "" }} value="EUR">EUR</option>
                            <option {{ ($rezervasyon->rezervasyon_toplam_satis_doviz == "GBP") ? "selected" : "" }} value="GBP">GBP</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="form-group row">
                    <label for="toplamSatis" class="col-4 col-form-label">Kapora</label>
                    <div class="col-4">
                        <input class="form-control" type="text" value="{{ $rezervasyon->rezervasyon_kapora }}" id="kapora" name="kapora">
                    </div>
                    <div class="col-4">
                        <select class="form-control" id="kaporaD" name="kaporaD">
                            <option {{ ($rezervasyon->rezervasyon_kapora_doviz == "TRY") ? "selected" : "" }} value="TRY">TRY</option>
                            <option {{ ($rezervasyon->rezervasyon_kapora_doviz == "USD") ? "selected" : "" }} value="USD">USD</option>
                            <option {{ ($rezervasyon->rezervasyon_kapora_doviz == "EUR") ? "selected" : "" }} value="EUR">EUR</option>
                            <option {{ ($rezervasyon->rezervasyon_kapora_doviz == "GBP") ? "selected" : "" }} value="GBP">GBP</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="form-group row">
                    <label for="toplamSatis" class="col-4 col-form-label">Kalan Rest</label>
                    <div class="col-4">
                        <input class="form-control" type="text" value="{{ $rezervasyon->rezervasyon_rest }}" id="rest" name="rest">
                    </div>
                    <div class="col-4">
                        <select class="form-control" id="restD" name="restD">
                            <option {{ ($rezervasyon->rezervasyon_rest_doviz == "TRY") ? "selected" : "" }} value="TRY">TRY</option>
                            <option {{ ($rezervasyon->rezervasyon_rest_doviz == "USD") ? "selected" : "" }} value="USD">USD</option>
                            <option {{ ($rezervasyon->rezervasyon_rest_doviz == "EUR") ? "selected" : "" }} value="EUR">EUR</option>
                            <option {{ ($rezervasyon->rezervasyon_rest_doviz == "GBP") ? "selected" : "" }} value="GBP">GBP</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="row">
                    <div class="col-12" style="margin-top: -1rem">
                        <hr>
                        <h1></h1>
                    </div>
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary">Rezervasyon Düzenle</button>
                        <input type="reset" class="btn btn-warning" value="Sıfırla">
                    </div>
                </div>
            </div>
        </div>

    </form>
@endsection