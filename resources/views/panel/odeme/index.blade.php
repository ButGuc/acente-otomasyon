@extends('layouts.app')
@section('content')
    @if (session('status'))
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-info">
                    {{session('status')}}
                </div>
            </div>
        </div>
    @endif
    @foreach ($errors->all() as $error)
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-danger">
                    {{ $error }}
                </div>
            </div>
        </div>
    @endforeach

<div class="row">
    <div class="col-12">
        <h3>Ödemelerin Detayları</h3>
        <hr>
    </div>
</div>
<form action="{{url('/panel/odemeler/filitreli')}}" method="get">
    {{csrf_field()}}
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group row">
                <label for="carihesap" class="col-4 col-form-label">Cari Hesap</label>
                <div class="col-8">
                    <select class="form-control" id="carihesap" name="carihesap">
                        @forelse($ch as $item)
                            <option {{ (old('carihesap') == $item->ch_id) ? "selected" : "" }} value="{{$item->ch_id}}">{{$item->ch_adi}}</option>
                        @empty
                            <option>Boş</option>
                        @endforelse
                    </select>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group row">
                <label for="tarih" class="col-2 col-form-label">Tarih</label>
                <div class="col-4">
                    <input class="form-control" type="date" value="{{ old('tarih1') }}" id="tarih1" name="tarih1">
                </div>
                <div class="col-2">
                    <label for="tarih2" class="col-2 col-form-label"> Arası </label>
                </div>
                <div class="col-4">
                    <input class="form-control" type="date" value="{{ old('tarih2') }}" id="tarih2" name="tarih2">
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-12">
            <div class="row">
                <div class="col-12" style="margin-top: -1rem">
                    <hr>
                </div>
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-sm btn-primary">Ödemeleri Filitrele</button>
                    <input type="reset" class="btn btn-sm btn-warning" value="Sıfırla">
                    <a class="btn btn-sm btn-success {{ (count($odeme) <= 0) ? "disabled" : "" }}" href="{{ url('/panel/odeme/excel/').'?carihesap='.old('carihesap').'&tarih1='.old('tarih1').'&tarih2='.old('tarih2')}}"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel Çıktısı Al</a>
                </div>
            </div>
        </div>
        <div class="col-12"><hr></div>
    </div>
</form>
<div class="row">
    <div class="col-sm-12">
        <p><strong class="text-danger">Kırmızı</strong> renkteki satırlar Yapılan Ödemeyi, <strong class="text-success">Yeşil</strong> satırlar Alınan Ödemeyi temsil eder.</p>
        <div class="table-responsive">
            <table class="table table-bordered ">
                <thead>
                <tr>
                    <th class="text-center">Cari Hesap</th>
                    <th class="text-center">Açıklama</th>
                    <th class="text-center">Tarih</th>
                    <th class="text-center">Tutar</th>
                    <th class="text-center">Döviz</th>
                    <th class="text-center"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
                    <th class="text-center"><i class="fa fa-trash-o" aria-hidden="true"></i></th>
                </tr>
                </thead>
                <tbody>
                @forelse ($odeme as $o)
                    <tr class="{{ ($o->odeme_turu) ? "table-success" : "table-danger" }}">
                        <td>{{$o->Cari_Hesaplar->ch_adi}}</td>
                        <td>{{$o->odeme_aciklama}}</td>
                        <td>{{$o->odeme_tarihi}}</td>
                        <td>{{$o->odeme_tutar}}</td>
                        <td>{{$o->odeme_doviz}}</td>

                        <td class="text-center"><a class="btn btn-sm btn-outline-warning" href="{{ url('/panel/odeme/duzenle/'.$o->odeme_id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                        <td class="text-center"><a class="btn btn-sm btn-outline-danger" href="{{ url('/panel/odeme/sil/'.$o->odeme_id) }}"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                    </tr>
                @empty
                    <p>Boş</p>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection