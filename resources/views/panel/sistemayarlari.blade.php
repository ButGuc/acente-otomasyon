@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <h3>Sistem Ayarları</h3>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-md-6">
        <div class="row">
            <div class="col-6">
                <h4>Oteller</h4>
            </div>
            <div class="col-3">
                <a class="btn btn-sm btn-block btn-outline-success" href="{{ url('/panel/otel/ekle') }}">Ekle</a>
            </div>
            <div class="col-3">
                <a class="btn btn-sm btn-block btn-outline-warning" href="{{ url('/panel/oteller') }}">Düzenle</a>
            </div>
        </div>
        <hr>
    </div>
    <div class="col-sm-12 col-md-6">
        <div class="row">
            <div class="col-6">
                <h4>Turlar</h4>
            </div>
            <div class="col-3">
                <a class="btn btn-sm btn-block btn-outline-success" href="{{ url('/panel/tur/ekle') }}">Ekle</a>
            </div>
            <div class="col-3">
                <a class="btn btn-sm btn-block btn-outline-warning" href="{{ url('/panel/turlar') }}">Düzenle</a>
            </div>
        </div>
        <hr>
    </div>
    <div class="col-sm-12 col-md-6">
        <div class="row">
            <div class="col-6">
                <h4>Rehberler</h4>
            </div>
            <div class="col-3">
                <a class="btn btn-sm btn-block btn-outline-success" href="{{ url('/panel/rehber/ekle') }}">Ekle</a>
            </div>
            <div class="col-3">
                <a class="btn btn-sm btn-block btn-outline-warning" href="{{ url('/panel/rehberler') }}">Düzenle</a>
            </div>
        </div>
        <hr>
    </div>
    <div class="col-sm-12 col-md-6">
        <div class="row">
            <div class="col-6">
                <h4>Alınış Saatleri</h4>
            </div>
            <div class="col-3">
                <a class="btn btn-sm btn-block btn-outline-success" href="{{ url('/panel/asb/ekle') }}">Ekle</a>
            </div>
            <div class="col-3">
                <a class="btn btn-sm btn-block btn-outline-warning" href="{{ url('/panel/asb') }}">Düzenle</a>
            </div>
        </div>
        <hr>
    </div>
    <div class="col-sm-12 col-md-6">
        <div class="row">
            <div class="col-6">
                <h4>Döviz Kurları</h4>
            </div>
            <div class="col-6">
                <a class="btn btn-sm btn-block btn-outline-primary" href="{{ url('/panel/dovizDetay') }}">Detay</a>
            </div>
        </div>
        <hr>
    </div>
</div>
@endsection