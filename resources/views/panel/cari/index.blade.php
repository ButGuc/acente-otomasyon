@extends('layouts.app')
@section('head')
    <script>
        $(function(){
            $('#carihesap').on('change', function () {
                var url = $(this).val();
                if (url) { window.location = "{{ url("/panel/caridetaylari") }}/"+url; }
                return false;
            });
        });
    </script>
@endsection
@section('content')
    @if (session('status'))
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-info">
                    {{session('status')}}
                </div>
            </div>
        </div>
    @endif
    @foreach ($errors->all() as $error)
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-danger">
                    {{ $error }}
                </div>
            </div>
        </div>
    @endforeach
<div class="row">
    <div class="col-12">
        <h3>Cari Detayları</h3>
        <hr>
    </div>
</div>
<form action="{{url('/panel/caridetaylari/filitreli')}}" method="get">
    {{csrf_field()}}
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group row">
                <label for="carihesap" class="col-4 col-form-label">Cari Hesap</label>
                <div class="col-8">
                    <select class="form-control" id="carihesap" name="carihesap">
                        <option>Lütfen Bir Hesap Seçin</option>
                        @forelse($ch as $item)
                            <option {{ ($chid == $item->ch_id) ? "selected" : "" }} value="{{$item->ch_id}}">{{$item->ch_adi}}</option>
                        @empty
                            <option>Boş</option>
                        @endforelse
                    </select>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group row">
                <label for="tarih" class="col-2 col-form-label">Tarih</label>
                <div class="col-4">
                    <input class="form-control" type="date" value="{{ old('tarih1') }}" id="tarih1" name="tarih1">
                </div>
                <div class="col-2">
                    <label for="tarih2" class="col-2 col-form-label"> Arası </label>
                </div>
                <div class="col-4">
                    <input class="form-control" type="date" value="{{ old('tarih2') }}" id="tarih2" name="tarih2">
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-12">
            <div class="row">
                <div class="col-12" style="margin-top: -1rem">
                    <hr>
                </div>
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-sm btn-primary">Carileri Filitrele</button>
                    <input type="reset" class="btn btn-sm btn-warning" value="Sıfırla">
                    <a class="btn btn-sm btn-success {{ (count($output) <= 0) ? "disabled" : "" }}"
                       href="{{ (empty(old('carihesap'))) ? url('/panel/cari/excel')."/".$chid : url('/panel/cari/excel/').'?carihesap='.old('carihesap').'&tarih1='.old('tarih1').'&tarih2='.old('tarih2') }}">
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel Çıktısı Al
                    </a>
                </div>
            </div>
        </div>
        <div class="col-12"><hr></div>
    </div>
</form>
<div class="row">
    <div class="col-sm-12">
        <p><strong class="text-danger">Kırmızı</strong> bakiyenin ekside olduğunu, <strong class="text-success">Yeşil</strong> bakiyenin artıda olduğunu temsil eder. <strong class="text-info">?</strong> işareti Rezervasyon detayını gösterir.</p>
        <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th class="text-center">Tarih</th>
                    <th class="text-center">Tur</th>
                    <th class="text-center">Y</th>
                    <th class="text-center">Ç</th>
                    <th class="text-center">B</th>
                    <th class="text-center">Ü</th>
                    <th class="text-center">Borç</th>
                    <th class="text-center">Alacak</th>
                    <th class="text-center">Bakiye</th>
                    <th class="text-center">Toplam</th>
                    <th class="text-center"><i class="fa fa-question-circle" aria-hidden="true"></i></th>
                    <th class="text-center"><i class="fa fa-times-circle" aria-hidden="true"></i></th>
                </tr>
                </thead>
                <tbody>
                @php $sayac = 1; $toplam = (float)0; @endphp
                @forelse($output as $item)
                    @php $sayac++; $toplam += $item->bakiye; @endphp
                    <tr style="{{ ($item->type == 0) ? "color: #387eff" : "" }}" class="{{ ($item->bakiye > 0) ? "table-success" : "table-danger" }}">
                        <td>{{ $sayac }}</td>
                        <td>{{ $item->tarih }}</td>
                        @if($item->type == 1)
                            <td>{{ $item->tur }}</td>
                            <td>{{ $item->y }}</td>
                            <td>{{ $item->c }}</td>
                            <td>{{ $item->b }}</td>
                            <td>{{ $item->u }}</td>
                            <td>{{ $item->borc }}</td>
                            <td>{{ $item->alacak }}</td>
                            <td>{{ $item->bakiye }}</td>
                            <td>{{ $toplam }}</td>
                            <td class="text-center"><a class="btn btn-sm btn-outline-info" href="{{ url('/panel/rezervasyon/detay/'.$item->rez) }}"><i class="fa fa-question-circle" aria-hidden="true"></i></a></td>
                            <td class="text-center"><a class="btn btn-sm btn-outline-danger" href="{{ url('/panel/cari/sil/'.$item->id) }}"><i class="fa fa-times-circle" aria-hidden="true"></i></a></td>
                        @else
                            <td>{{ ($item->odeme == 0) ? "Yapıldı" : "Alındı" }}</td>
                            <td colspan="5">{{ $item->aciklama }}</td>
                            <td>{{ $item->tutar }}</td>
                            <td>{{ $item->bakiye }}</td>
                            <td>{{ $toplam }}</td>
                            <td class="text-center"><a class="btn btn-sm btn-outline-warning" href="{{ url('/panel/odeme/duzenle/'.$item->id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                            <td class="text-center"><a class="btn btn-sm btn-outline-danger" href="{{ url('/panel/odeme/sil/'.$item->id) }}"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                        @endif
                    </tr>
                @empty
                    <p>Boş</p>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection