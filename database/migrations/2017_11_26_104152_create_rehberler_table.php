<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRehberlerTable extends Migration
{

    public function up()
    {
        Schema::create('rehberler', function (Blueprint $table) {
            $table->increments('rehber_id');
            $table->char('rehber_adi', 100);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('rehber');
    }
}
