<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsbTable extends Migration
{

    public function up()
    {
        Schema::create('asb', function (Blueprint $table) {
            $table->increments('asb_id');
            $table->char('asb_value', 100);
            $table->integer('tur_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('asb', function($table)
        {
            $table->foreign('tur_id','fk_asb_tur')->references('tur_id')->on('turlar')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('asb');
    }
}
