<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRezervasyonlarTable extends Migration
{

    public function up()
    {
        Schema::create('rezervasyonlar', function (Blueprint $table) {
            $table->increments('rezervasyonlar_id');
            $table->char('rezervasyon_no', 50);
            $table->date('rezervasyon_tarihi');
            $table->char('rezervasyon_adi', 50);
            $table->char('rezervasyon_tel', 50);
            $table->char('rezervasyon_oda_no', 50);
            $table->integer('rezervasyon_toplam_satis');
            $table->char('rezervasyon_toplam_satis_doviz', 3);
            $table->integer('rezervasyon_kapora');
            $table->char('rezervasyon_kapora_doviz', 3);
            $table->integer('rezervasyon_rest');
            $table->char('rezervasyon_rest_doviz', 3);
            $table->smallInteger('rezervasyon_yetiskin_pax');
            $table->smallInteger('rezervasyon_cocuk_pax');
            $table->smallInteger('rezervasyon_bebek_pax');
            $table->smallInteger('rezervasyon_ucret_pax');
            $table->integer('otel_id')->unsigned();
            $table->integer('tur_id')->unsigned();
            $table->integer('rehber_id')->unsigned();
            $table->integer('asb_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('rezervasyonlar', function($table)
        {
            $table->foreign('otel_id','fk_rez_otel')->references('otel_id')->on('oteller')->onDelete('cascade');
            $table->foreign('tur_id','fk_rez_tur')->references('tur_id')->on('turlar')->onDelete('cascade');
            $table->foreign('rehber_id','fk_rez_rehber')->references('rehber_id')->on('rehberler')->onDelete('cascade');
            $table->foreign('asb_id','fk_rez_asb')->references('asb_id')->on('asb')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('rezervasyonlar');
    }
}
