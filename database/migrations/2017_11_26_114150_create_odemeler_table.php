<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOdemelerTable extends Migration
{
    public function up()
    {
        Schema::create('odemeler', function (Blueprint $table) {
            $table->increments('odeme_id');
            $table->boolean('odeme_turu');
            $table->char('odeme_aciklama', 50);
            $table->float('odeme_tutar');
            $table->float('odeme_tutar_cari');
            $table->char('odeme_doviz', 3);
            $table->date('odeme_tarihi');
            $table->integer('ch_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('odemeler', function($table)
        {
            $table->foreign('ch_id','fk_odeme_ch')->references('ch_id')->on('cari_hesaplar')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('odemeler');
    }
}
