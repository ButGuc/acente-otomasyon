<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtellerTable extends Migration
{

    public function up()
    {
        Schema::create('oteller', function (Blueprint $table) {
            $table->increments('otel_id');
            $table->char('otel_adi', 100);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('oteller');
    }
}
