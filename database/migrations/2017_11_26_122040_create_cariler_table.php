<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarilerTable extends Migration
{

    public function up()
    {
        Schema::create('cariler', function (Blueprint $table) {
            $table->increments('cari_id');
            $table->date('cari_tarihi');
            $table->float('cari_borc');
            $table->float('cari_alacak');
            $table->float('cari_bakiye');
            $table->integer('otel_id')->unsigned();
            $table->integer('tur_id')->unsigned();
            $table->integer('rezervasyonlar_id')->unsigned();
            $table->integer('ch_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('cariler', function($table)
        {
            $table->foreign('otel_id','fk_cari_otel')->references('otel_id')->on('oteller')->onDelete('cascade');
            $table->foreign('tur_id','fk_cari_tur')->references('tur_id')->on('turlar')->onDelete('cascade');
            $table->foreign('rezervasyonlar_id','fk_cari_rezervasyon')->references('rezervasyonlar_id')->on('rezervasyonlar')->onDelete('cascade');
            $table->foreign('ch_id','fk_cari_ch')->references('ch_id')->on('cari_hesaplar')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('cariler');
    }
}
